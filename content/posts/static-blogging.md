+++
title = "first touch with static blogging"
author = ["matteo"]
date = 2020-04-08
tags = ["personal", "hugo"]
categories = ["updates"]
draft = true
+++

Here is an equation:

{{< figure src="/ox-hugo/test.png" >}}

test image 1234 :

{{< figure src="/ox-hugo/test1236.png" >}}

blow a test of exported code block

```python
def main();
    pass
```