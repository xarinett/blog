+++
title = "Linear algebra basics notes 1 vector multiplication with numpy"
author = ["matteo"]
date = 2020-04-24
tags = ["numpy", "python"]
categories = ["algebra"]
draft = false
+++

Some days ago I started to revise the some Linear Algebra, from some old book that i had in university, and i configured emacs to execute the numpy code.
At the beginning i try to use it with jupiter, but as I prefears some interface that can be more flexible. I configured an emacs package called ein (emacs ipython notebook). I found that interface interesting and really useful for his simplicity but finally my choise gone to use directly ORG mode. All of that tool that i mentioned are valid but for the workflow that I like to have i think that ORG is the right choice.


## Define vector and matrix {#define-vector-and-matrix}

A vector is an array, numpy have his specific way to define a vector.

{{< figure src="/images/vector_example.png" >}}

The follow code will import matplotlib and numpy.

```python
%matplotlib inline
%config InlineBackend.figure_format = 'png'
import matplotlib.pyplot as plt
import numpy as np
np
```

```text
<module 'numpy' from '/usr/lib/python3/dist-packages/numpy/__init__.py'>
```

The for example to create a vector that will contains the values from 1 to 10 :

```python
np.arange(1, 10)
```

```text
array([1, 2, 3, 4, 5, 6, 7, 8, 9])
```

Also define a matrix is simple with numpy

{{< figure src="/images/matrix_example.png" >}}

we can define a matrix directly by specify a numpy array

```python
#identity matrix example
a=np.array([[1,0,0],[0,1,0],[0,0,1]])
np.identity(3)
```

```text
array([[1., 0., 0.],
       [0., 1., 0.],
       [0., 0., 1.]])
```

numpy also provide methods to multiply vectors for example, let define two vectors a and b.

```python
va=np.array([1,2,3])
vb=np.array([3,2,1])
(va,vb)
```

```text
(array([1, 2, 3]), array([3, 2, 1]))
```


## Vector and Matrix multiplication {#vector-and-matrix-multiplication}

Matrix multiplication is defined as :

{{< figure src="/images/matrix_product.png" >}}

A direct multiplication of two vector in numpy is an **element-wise product**.

```python
va*vb
```

```text
array([3, 4, 3])
```

also _multiply_ will return the element-wise product

```python
np.multiply(va,vb)
```

```text
array([3, 4, 3])
```

If we want apply the **dot product**

```python
np.outer(va,vb)
```

```text
array([[3, 2, 1],
      [6, 4, 2],
      [9, 6, 3]])
```

for matrix is similar let define two matrix A and B

```python
A=np.matrix([[1,2,3],[1,2,3],[1,2,3]])
B=np.matrix([[2,1,3],[4,5,6],[2,1,1]])

(A,B)
```

```text
(matrix([[1, 2, 3],
[1, 2, 3],
[1, 2, 3]]),
matrix([[2, 1, 3],
[4, 5, 6],
[2, 1, 1]]))
```

_element-wise_ multiplication can be done :

```python
np.multiply(A,B)
```

```text
matrix([[ 2,  2,  9],
        [ 4, 10, 18],
        [ 2,  2,  3]])
```

_Dot product_ instead is done by calling dot method

```python
A.dot(B)
```

```text
matrix([[16, 14, 18],
       [16, 14, 18],
       [16, 14, 18]])
```