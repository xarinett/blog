+++
title = "Linear algebra notes 2 basics"
author = ["matteo"]
date = 2020-04-12
tags = ["numpy", "python"]
categories = ["algebra"]
draft = true
+++

\#+COMMENT

```nil

In [1]:
# this sheet will include all algebra from the book

import numpy as np

#vector from 1 to 10
np.arange(1, 10)
Out [1]:
array([1, 2, 3, 4, 5, 6, 7, 8, 9])

In [10]:
#identity matrix example
a=np.array([[1,0,0],[0,1,0],[0,0,1]])
np.identity(3)

Out [10]:
array([[1., 0., 0.],
       [0., 1., 0.],
       [0., 0., 1.]])

In [ ]:


In [11]:
va=np.array([1,2,3])
vb=np.array([3,2,1])
va*vb
np.multiply(va,vb)

#multiply 2 vector in algebra way is outer
AB=np.outer(va,vb)
AB
Out [11]:
array([[3, 2, 1],
       [6, 4, 2],
       [9, 6, 3]])

In [36]:
# column vector
row=np.array([1,2,3]) # only one dimension
column=row.reshape(-1,1)
row.transpose().shape
row.shape
column.shape
row_second_dimension=row.reshape(1,-1)
row_second_dimension.shape
#print(row)
row[1]

# set
set(row)
Out [36]:
{1, 2, 3}

In [110]:
# Distributive A(B+C) = AB+AC
A=np.matrix([[1,2,3],[1,2,3],[1,2,3]])
B=np.matrix([[2,1,3],[4,5,6],[2,1,1]])
C=A

# sum of 2 matrix
A+B

# Distributive test
SUM1=A.dot(B+C)
SUM2=(A.dot(B))+(A.dot(C))

#A * B
(SUM1==SUM2).all()

#element-wise multiplication for matrix is
np.multiply(A,B)

#associative
((A*B)*C == A*(B*C)).all()

# not commutative
A*B == B*A




# (A*B)T = (BT*AT)
((A*B).T == B.T*A.T).all()


# vector is conmutative but, if we multipy 2 vectors without transpose the firstone we will have in reply the scalar multiplications. if we translpose them we will have the matrix shape as results as expected

va=np.array([[1,2,3]])
vb=va
va.T * vb

vb.T * va

Out [110]:
array([[1, 2, 3],
       [2, 4, 6],
       [3, 6, 9]])

In [181]:
# Ax = b
# Identity matrix
I=np.matrix(np.identity(3))

# solve matrix
A=np.matrix([[1,1,1],[0,1,0],[0,0,1]])
vb = np.array([2,1,1])
np.linalg.solve(A,vb)

# inverse matrix

A=np.matrix([[1,2,3],[1,2,2],[1,1,1]])

try:
    A_1=np.linalg.inv(A)
except np.linalg.LinAlgError:
    #not ivertible
    print("not invertible")
    pass

#that will return identity 3
((A * A_1) == np.identity(3)).all()


Out [181]:
True

In [131]:
# in python * is a element-wise multiplication
# matrix multiplication is .dot

#element-wise for arrays 2 dimensions
AB*AB
#matrix multiplication (element wise form matrix is np.multiply)
np.dot(AB,AB)
#vector multiplication
#np.outer(va,vb)

#vector multiplication
np.dot(va.T,vb)

#
(np.dot(A,B).T == np.dot(B.T,A.T)).all()

#
(A.T.T == A).all()

# x.T * y.T = (x.T* y).T = y.T * x

(((np.dot(va.T,vb) == (np.dot(va.T,vb).T))) == ((np.dot(va.T,vb)) == (np.dot(vb.T,va)))).all()



Out [131]:
True

In [ ]:
```

\#+END\_COMMENT