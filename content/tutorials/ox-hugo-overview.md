+++
title = "ox-hugo hugo exporter for ORG"
author = ["matteo"]
date = 2020-04-25
tags = ["emacs", "hugo", "org"]
categories = ["reviews"]
draft = false
+++

I think that Hugo is a great tool for static blogging, and one of the feature that moved me to test it was that it support native ORG files. After some test, I start at the beginning to create my proper shortcut to interact with hugo. Later I found that exist an Org exporter for hugo, called _ox-hugo_ and i finally moved to it.

Maybe at the beginning i was a bit confused and I don't really remember what was exactly my problem but _ox-hugo_ has increased my productivity. On my side i confess that I'm a bit new with Org, emacs and hugo.


## Install ox-hugo {#install-ox-hugo}

to install _ox-hugo_ is simple as run in emacs

```nil
M-x package install ox-hugo
```

then add to your .emacs the follow lines

```emacs-lisp

(with-eval-after-load 'ox
  (require 'ox-hugo))

(use-package ox-hugo
  :ensure t            ;Auto-install the package from Melpa (optional)
  :after ox)
```


## basic usage {#basic-usage}

at the beginning of your org file you may like to add :

```org

#+seq_todo: TODO DRAFT DONE
#+hugo_base_dir: <path of your hugo base dir>
```

first line will add to your basic TODO list the DRAFT tag. This tag will mean that the specific article will be created and deployed on your hugo site but will not be published to be public accessible.

second line is specify the hugo base folder of your site, it will export your org file to a set of articles in markdown format in the content folder.

| 0   |             |
|-----|-------------|
| ├── | archetypes  |
| ├── | content     |
| ├── | data        |
| ├── | layouts     |
| ├── | public      |
| ├── | resources   |
| ├── | static      |
| └── | themes      |
| 8   | directories |

Once this configuration is properly loaded we will be able to generate the pages in the content folder by :

```bash
C-c e H A
```

if at this time you have the hugo service loaded you will be able to see your blog at <https://localhost:1313>


## Notes {#notes}

In case you decide to delete or not publish some page and you delete them from the Org file, you should also delete manually the markdown file generated inside hugo contents.

in my case as I'm editing file in org mode i like sometime to disable the org confirmation when bable is try to evaluate the block codes, is easy to disable this feature just execute the follow elisp code.

```emacs-lisp

(setq org-confirm-babel-evaluate nil)
```