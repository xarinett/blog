+++
title = "Hugo easy posts with ORG"
author = ["matteo"]
date = 2020-04-11
tags = ["emacs", "hugo", "org"]
categories = ["reviews"]
draft = false
+++

During the easter week, also caused by the Covid-19 quarantine that we are experiencing in Spain i finally decided to start another blog.

After check some options, i was clear that for this personal blog i like a static solution, no web server, no application servers, db ..., mostly because i just spend part of my week debugging and configuring things like that an i like to have some more cheap and flexibility to publish my blog without have virtual servers or domains to pay. I will may post an article in to go more in deep of that decision.

Anyway i finally decided to use hugo for my static blog.
As this is the tutorial version i will try to be some more pragmatic, and the idea is to explain the basic steps that i followed to configure hugo to work with org document.

The good thing is that for use org in hugo you don't have to do anything special, it works out of the box. You can just create an example org post and it will be used by hugo to generate the html files in your _public_ folder.


## Install latest version of Hugo {#install-latest-version-of-hugo}

Then if you don't have just installe latest hugo version you can check the latest version here : <https://github.com/gohugoio/hugo/releases/>

```bash
wget https://github.com/gohugoio/hugo/releases/download/v0.69.0/hugo_0.69.0_Linux-64bit.deb

sudo dpkg -i hugo_0.69.0_Linux-64bit.deb

sudo apt-get  install -f
```

now you can check the installed version:

```bash
hugo version
```

you should see in the cosole :

```text
Hugo Static Site Generator v0.69.0-4205844B linux/amd64 BuildDate: 2020-04-10T09:12:34Z
```

Once you have installed hugo to init a new site you can launch :

```bash

hugo new site <sitename>
cd <sitename>
git init
git submodule add https://<your preferred hugo theme>.git themes/<theme name>
echo 'theme = "<theme name>"' >> config.toml
```

to check your site you could just start hugo server as :

```bash
hugo server -D
```

now you can check your blog at : <http://localhost:1313>


## Notes {#notes}

In case yout theme need taxonomy, tags and categories to be enabled  just add _meta = true_ inside the config.toml file.


## ORG file example in HUGO {#org-file-example-in-hugo}

Write ORG posts with HUGO is simple as to create a new ORG file in the folder _/blog-home/content/post_

for example you can just create with your preferred editor  _/content/post/my-first-post.org_

```text
---
title: "First Org Test"
date: 2020-04-05T11:29:15+02:00
draft: false
---

* my title
 *some text*
** a subtile
 /more text/
** another subtitle

or a table

|++++++++++|+++++++++++|
| header 1 | header 2  |
|++++++++++|+++++++++++|
|        1 |        2  |
|++++++++++|+++++++++++|
```