+++
title = "how to write LaTeX snippets in org mode to publish with ox-hugo"
author = ["matteo"]
date = 2020-04-09
tags = ["LaTex", "org", "emacs"]
categories = ["reviews"]
draft = true
+++

I think these gave me the herpes.

```latex

\begin{equation}                        % arbitrary environments,
E = -J \sum_{i=1}^N s_i s_{i+1}
\tag{5.23} \label{eq:special}           % even tables, figures
\end{equation}                          % etc
```

{{< figure src="/ox-hugo/test-formula.png" >}}

This is a first note seems that expansion for org sources is stop to work.
as workarround we can execute the follow elisp code.

```nil

(when (version<= "9.2" (org-version))
    (require 'org-tempo))
```

that is from the ox-hugo page

```nil

(with-eval-after-load 'ox
  (require 'ox-hugo))

(use-package ox-hugo
  :ensure t            ;Auto-install the package from Melpa (optional)
  :after ox)

(setq org-use-tag-inheritance 't)

(require 'org)
(require 'ox-latex)
(setq org-latex-create-formula-image-program 'dvipng)
(org-babel-do-load-languages 'org-babel-load-languages '((latex . t)))
```

blablabla

\#+COMMENT
At the moment the only information that i share is my gitlab account, you can find what i like to do in my space time here : <https://xarinett.gitlab.io/>
\#+END\_COMMENT