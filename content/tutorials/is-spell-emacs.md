+++
title = "use is-spell in Emacs"
author = ["matteo"]
date = 2020-04-09
draft = false
+++

## Install {#install}

One of the most useful things that can be configured in Emacs, and most for me that I'm not a _native English_ speaker is a spell checker. there are many ways to configure a spell checker in Emacs, what i proposed here in that notes is to use fly-check together with Aspell.

in the configuration the only thing that should be added is the ispell program that we want to use :

```emacs-lisp
(setq ispell-program-name "/usr/bin/aspell")
```

Flyspell is available in melpa, and can be installed just by typing :

```nil
M-x package-install flyspell
```

to install aspell and English dictionary

```nil
sudo apt install aspell  aspell-en
```

you can check that aspell is properly installed by run :

```bash
/usr/bin/aspell --version
```


## use aspell in emacs {#use-aspell-in-emacs}

First of all we should configure flyspell to use aspell dictionaries with by adding in .emacs file the follow line :

```emacs-lisp
(setq ispell-program-name "/usr/bin/aspell")
```

then to activate the spell check we should activate flyspell with by launching _M-x flyspell RET_ in emacs.
Once is activated we can see that some words will be underlined if are incorrect.

{{< figure src="/images/aspell-tutorial.png" >}}

basic usage is the follow

-   M-x ispell-word
    to check if a word is correct with the selected dictionary
-   M-x ispell-change-dictionary
    to select different dictionary
-   M-$ on the underlined word
    to see all choices, it will open a list at the top and is possible to replace just bu press the number of the choise
-   C-; on the incorrect word
    it will rotate the choices